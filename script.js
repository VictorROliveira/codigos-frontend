let family = {
  receitas: [2400, 250.5, 4000, 340.6],
  despesas: [300.5, 1500, 550.4]
}

function sum(Array) {
  let total = 0

  for (let value of Array) {
    total += value
  }

  return total
}

function calcularTotal(){
  const calcularReceitas = sum(family.receitas)
  const calcularDespesas = sum(family.despesas)

  const total = calcularReceitas - calcularDespesas
  const tudoOk = total >= 0 
  let mensagem = "Negativo"

  if (tudoOk) {
    mensagem = "positivo"
  }
 
  console.log(`Seu saldo é ${mensagem}: no valor de ${total.toFixed(2)}R$`)
}

calcularTotal()
